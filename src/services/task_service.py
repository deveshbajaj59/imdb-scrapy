import hashlib
from models import movie_data
import logging
import json
from datetime import datetime
from bs4 import BeautifulSoup
import requests
from .scraping import imdb_movies
import re


class TaskServices():
    """
    service function for user related business logic
    """

    def add_movies(self, my_data):
        try:
            print(my_data)
            obj = imdb_movies()
            all_movies = obj.get_alldata()
            # print(all_movies)
            if len(all_movies) == 0:
                return False
            for movies in all_movies:
                # print(movies)
                res = movie_data.Movie_Data(
                    title=movies['title'],
                    cast=movies["cast"],
                    genres=movies["genres"],
                    rating=movies["rating"],
                    language=movies["language"],
                    director=movies["director"],
                    country=movies["country"]
                ).save()
                print(res)
            return True
        except Exception as identifier:
            logging.exception(identifier)
            return False

    def fetch_alldata(self, data):
        all_movies = []
        print(data)
        if 'title' in data and 'genres' in data:
            search_data = movie_data.Movie_Data.objects(
                title__contains=data['title'],
                genres=data['genres']
            )
        elif 'title' in data:
            search_data = movie_data.Movie_Data.objects(
                title__contains=data['title']
            )
        elif 'genr)es' in data:
            search_data = movie_data.Movie_Data.objects(
                genres=data['genre']
            )
        else:
            return False
        # print(data)
        for i in search_data:
            all_movies.append(
                {
                    'title': i.title,
                    'language': i.language,
                    'director': i.director,
                    'country': i.country,
                    'rating': i.rating,
                    'genres': i.genres
                })
        print(len(all_movies))
        return all_movies

    def final_query(self, data):
        try:
            print(data)
            if data['data'] == "top_movies_by_language":
                pipeline = [{"$group":{"_id":{"$arrayElemAt": ["$language", 0]},"movies": { "$addToSet": "$_id" },"count":{"$sum":1}}},{ "$sort": { "count": -1 }}] 
                res = movie_data.Movie_Data.objects.aggregate(*pipeline)
                data =[i for i in res] 
                return data
            elif data['data'] == "top_20_movies_by_actor":
                pipeline = [{"$group":{"_id":{"$arrayElemAt": ["$cast", 0]},"movies": { "$addToSet": "$_id" },"count":{"$sum":1}}},{ "$sort": { "count": -1 }}]
                res = movie_data.Movie_Data.objects.aggregate(*pipeline)
                data =[i for i in res] 
                return data[:20]
            elif data['data'] == 'actor_acted_in_1_movie':
                pipeline = [{"$group":{"_id":{"$arrayElemAt": ["$cast", 0]},"movies": { "$addToSet": "$_id" },"count":{"$sum":1}}},{ "$sort": { "count": -1 }}]
                res = movie_data.Movie_Data.objects.aggregate(*pipeline)
                data=[]
                for i in res:
                    if i['count'] ==1:
                        data.append(i)
                return data
            elif data['data'] == "top_10_director":
                pipeline = [{"$group":{"_id":{"$arrayElemAt": ["$director", 0]},"count":{"$sum":1}}},{ "$sort": { "count": -1 }}]
                res = movie_data.Movie_Data.objects.aggregate(*pipeline)
                data =[i for i in res] 
                return data[:10]

        except Exception as identifier:
            logging.exception(identifier)
            return False
        
        # top_movies_by_language = """db.movie__data.aggregate([{"$group":{"_id":{"$arrayElemAt": ["$language", 0]},count:{$sum:1}}}])"""
        # top_movies_by_actors = """db.movie__data.aggregate([{"$group":{"_id":{"$arrayElemAt": ["$cast", 0]},count:{$sum:1}}},{ $sort: { count: -1 }}])"""
