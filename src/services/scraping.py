from bs4 import BeautifulSoup
import requests
import re
from pprint import pprint
BASEURL = "https://www.imdb.com"


class imdb_movies():
    def fetch_from_fullcast(self, url):
        url = BASEURL+url
        print("fetching from {} ".format(url))
        response = requests.get(url)
        soup = BeautifulSoup(response.content, 'html.parser')
        # full_url = soup.find_all(
        #     "div", {"class": "plot_summary"})[-1].find_all('a')[-1]['href']

        movie_data = {}

        # geners
        res = soup.find_all("div", {"class": "see-more inline canwrap"})
        movie_data['genres'] = []
        # movie_data['geners'] = [i.text.strip() for i in res.find_all('a')]
        for tags in res:
            if tags.find('h4').text == "Genres:":
                movie_data['genres'] += [j.text.strip()
                                         for j in tags.find_all('a')]
        # /geners
        # lang and country
        res = soup.find("div", {"id": "titleDetails"}).find_all(
            'div', {"class", "txt-block"})
        movie_data['language'] = []
        movie_data['country'] = []
        for i in res:
            if i.find('h4') is not None:
                if i.find('h4').text == "Language:":
                    movie_data['language'] += [j.text.strip()
                                               for j in i.find_all('a')]
                if i.find('h4').text == "Country:":
                    movie_data['country'] += [j.text.strip()
                                              for j in i.find_all('a')]

        # movie_data['language'] = res[1].find('a').text
        # movie_data['country'] = res[0].find('a').text
        # / lang and country
        url = url+"fullcredits/"
        print("fetching from {} ".format(url))
        response = requests.get(url)
        soup = BeautifulSoup(response.content, 'html.parser')

        # cast
        res = soup.find("table", {"class": "cast_list"})
        res = res.find_all('tr')
        all_cast = []
        for i in res:
            data = i.find_all('td')
            if len(data) > 1:
                cast = data[1].find('a').text.strip()
                all_cast.append(cast)
        movie_data['cast'] = all_cast
        # ./cast

        res = soup.find_all("table", {"class": "simpleTable simpleCreditsTable"})[
            0].find_all('td', {"class": "name"})
        movie_data['director'] = [i.find('a').text.strip() for i in res]
        return movie_data

    def get_alldata(self):
        url = "https://www.imdb.com/india/top-rated-indian-movies/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=600ca544-31f5-4bd8-ae38-ea4014c93bab&pf_rd_r=0JYDCR65ERA8DWGHNC30&pf_rd_s=right-4&pf_rd_t=15061&pf_rd_i=homepage&ref_=hm_india_tr_rhs_1"

        response = requests.get(url)
        soup = BeautifulSoup(response.content, 'html.parser')
        res = soup.find("tbody", {"class": "lister-list"})
        res = res.find_all("tr")
        all_data = []
        for rows in res[:40]:
            title = rows.find("td", {"class": "titleColumn"})
            movie_details = {}
            url = title.find('a')['href']
            movie_details['title'] = title.find('a').text               # Name
            print("*"*10)
            print(movie_details['title'])
            rating = rows.find(
                "td", {"class": "ratingColumn imdbRating"}).find("strong")
            movie_details['rating'] = rating.text               # rating.text
            # data = fetch_movie_details(url)
            data = self.fetch_from_fullcast(url)
            movie_details.update(data)

            print(movie_details)
            all_data.append(movie_details)
        return all_data
