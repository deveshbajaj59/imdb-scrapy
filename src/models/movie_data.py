from mongoengine import Document, StringField, ListField


class Movie_Data(Document):
    """
    TASK: Create a model for task with minimalists
                    information required for user authentication
    """
    title = StringField(max_length=100, required=True, primary_key=True)
    cast = ListField(max_length=50)
    genres = ListField(max_length=50)
    rating = StringField(max_length=50)
    language = ListField(max_length=50)
    director = ListField(max_length=50)
    country = ListField(max_length=50)
