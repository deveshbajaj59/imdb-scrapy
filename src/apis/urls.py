from apis import views


api_urls = [
    ("/custom_query", views.custom_query, ["POST"],
        "search data in db"),
    ("/search_data", views.search_data, ["POST"],
        "search data in db"),
    ("/add_data", views.add_movie_data, ["POST"],
        "add movie item in DB"),
    ("/index", views.index_page, ["GET"],
        "index html")
]

other_urls = []

all_urls = api_urls + other_urls
