import logging
from flask import request
import json
import logging
from services import task_service
import urllib.parse
from flask import render_template

logger = logging.getLogger("default")


def add_movie_data():
    """
        insert movie data in Database
    """
    try:
        print(request.get_data())
        raw_data = json.loads(request.get_data().decode())
        print(raw_data, type(raw_data))
        task_obj = task_service.TaskServices()
        res = task_obj.add_movies(raw_data)
        return str(res)

    except Exception as identifier:
        logging.exception(identifier)
        return "500 Internal Server Error"


def search_data():
    """
        search data in database
    """
    try:
        print(request.get_data())
        raw_data = json.loads(request.get_data().decode())
        print(raw_data, type(raw_data))
        task_obj = task_service.TaskServices()
        res = task_obj.fetch_alldata(raw_data)
        return json.dumps(res)

    except Exception as identifier:
        logging.exception(identifier)
        return "500 Internal Server Error"


def custom_query():
    """
        making custom query
    """
    try:
        print(request.get_data())
        raw_data = json.loads(request.get_data().decode())
        print(raw_data, type(raw_data))
        task_obj = task_service.TaskServices()
        res = task_obj.final_query(raw_data)
        return json.dumps(res)
    except Exception as identifier:
        logging.exception(identifier)
        return "500 Internal Server Error"

def index_page():
    return render_template("index.html")
