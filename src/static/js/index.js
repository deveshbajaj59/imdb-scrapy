function makerestcall(){
    // alert("toyoyo") 
    alert("inserting data in database")
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
         if (this.readyState == 4 && this.status == 200) {
             if (this.responseText == "True"){
                 alert('Data Inserted sucessfully , can be searched');
             }
             else{
                alert('Error while scraping data , make sure your url is similar to this \n https://www.imdb.com/list/ls003992425/ ');
             }
         }
    };
    xhttp.open("POST", "/add_data", false);
    xhttp.setRequestHeader("Content-type", "application/json");
    doc ={
        "url":"tmp"
    }
    xhttp.send(JSON.stringify(doc));
}

function check_action(){
    query = document.getElementById("query").value; 
    console.log(query)
    if (query !="None")
    {
        res = displayQueries(query)
        if (res === true)
            return true
    }
    else
    {
        fetchdata()
    }
        
}

function fetchdata(){
    text = document.getElementById("text").value;
    genre = document.getElementById("genre").value;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
         if (this.readyState == 4 && this.status == 200) {
             if (this.responseText == "False"){
                alert('error in query');
             }
             else{
                // alert(this.responseText);
                maketable(JSON.parse(this.responseText))
             }
         }
    };
    xhttp.open("POST", "/search_data", false);
    xhttp.setRequestHeader("Content-type", "application/json");
    doc = {}
    if (text != '')
        doc['title'] =text
    if (genre != '')
        doc['genres'] =genre
    xhttp.send(JSON.stringify(doc));
}

function maketable(results){
    var table = document.getElementById("display_movies")
    table.innerHTML = "";
    var row = table.insertRow(0);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    var cell5 = row.insertCell(4);
    var cell6 = row.insertCell(5);
    var cell7 = row.insertCell(6);
    cell1.innerHTML = 'Sr';
    cell2.innerHTML = 'name';
    cell3.innerHTML = 'language';
    cell4.innerHTML = 'director';
    cell5.innerHTML = 'country';
    cell6.innerHTML ='rating'
    cell7.innerHTML ='genres'
    
    for (i in results){
        console.log(i)
        var row = table.insertRow(parseInt(i)+1);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var cell5 = row.insertCell(4);
        var cell6 = row.insertCell(5);
        var cell7 = row.insertCell(5);
        cell1.innerHTML = i;
        cell2.innerHTML = results[i]['title'];
        cell3.innerHTML = results[i]['language'];
        cell4.innerHTML = results[i]['director'];
        cell5.innerHTML = results[i]['country'];
        cell6.innerHTML = results[i]['genres'];
        cell7.innerHTML =results[i]['rating'];


    }
}

function displayQueries(data){
    alert(data)
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
         if (this.readyState == 4 && this.status == 200) {
             if (this.responseText == "False"){
                alert('error in query');
             }
             else{
                alert(this.responseText);
                return(make_table_custom(JSON.parse(this.responseText),data))

             }
         }
    };
    xhttp.open("POST", "/custom_query", false);
    xhttp.setRequestHeader("Content-type", "application/json");
    doc = {}
    doc['data'] =data
    xhttp.send(JSON.stringify(doc));
}

function make_table_custom(results,data){
    var table = document.getElementById("display_movies")
    table.innerHTML = "";
    var row = table.insertRow(0);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    cell1.innerHTML = 'Sr';
    cell2.innerHTML = data;
    cell3.innerHTML = 'Count';
    cell4.innerHTML = 'Movies';
    for (i in results){
        console.log(i)
        var row = table.insertRow(parseInt(i)+1);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        cell1.innerHTML = i;
        cell2.innerHTML = results[i]['_id'];
        cell3.innerHTML = results[i]['count'];
        cell4.innerHTML = results[i]['movies'];
    }
    return true
}