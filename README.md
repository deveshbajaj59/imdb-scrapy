## Imdb movie scraping and query app 

***Code Navigation***

This project consists of a main folder `src` which contains all the project files. The files outside the source folder consists of `docker-compose.yml`, `Dockerfile` and a `local.env` file which are used for running the docker containers. The `src` folder contains the entrypoint of the Flask project `app.py`, `settings` folder which contains your application settings and an `apis` folder that stores all your urls and views.
Template folder contains `index.html` and static contain `css` and `js` files

Specify the urls in `urls.py` file in the api folder as list of tuples with following format:

    (endpoint, view_func, methods, description)
example:

    ("/", views.index, ["GET"], "index page")



To run both the server run for Flask and MongoDb

    sudo docker-compose build
and then

    sudo docker-compose up
This will run the server at port http://localhost:8400/index

The index view will be displayed in your browser.


***functionality***
Click on Load data into database this will load first 40 movies in mongodb database for query

Go through the structure of the project 

logic is in api layer (i.e. views.py) and all the business logic is in the service layer (i.e. user_service.py).
Schema goes in the models/*.py file.

Refer docstrings in the above mentioned files and function 

***Improvement or Suggestion***

As of now I am using Flask and mongodb as web/app server and database respectively .
    
![alt text](./the_ui.png)

* Application can be more divide into 3 tire Arch for web server React/Angular can be used for ease and simplecity and to avoid complex js in Frontend.

